{{- define "backup-job-chart.osmConfig" -}}
contexts:
- config:
    access_key_id: {{ .Values.s3.accessKeyId }}
    auth_type: accesskey
    region: ap-south-1
    secret_key: {{ .Values.s3.secret }}
  name: objectstore
  provider: s3
current-context: objectstore
{{- end -}}