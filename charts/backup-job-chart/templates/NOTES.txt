This is a post-install note

{{- if and .Values.dbCreds.username .Values.dbCreds.password }}
Credentials were taken from provided values files
{{- else if .Values.dbCredsSecret }}     
Credentials were taken from secret " {{.Values.dbCredsSecret }}"
{{- else }}
!!!!!!!!! WARNING, NO DB CREDS WERE PROVIDED !!!!!!!!!
{{- end}}

